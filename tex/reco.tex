%! TEX program = lualatex

\documentclass[../thesis.tex]{subfiles}

\graphicspath{{fig/}{../fig/}{fig/reco/}{../fig/reco/}}

\makeatletter
\def\input@path{{fig/}{../fig}{../fig/reco/}{fig/reco/}}
\makeatother

\begin{document}

\chapter{Event Reconstruction}\label{chap:reco}

\epigraph{\textit{To achieve great things, two things are needed: a plan, and not quite enough time.}}{\textsc{Leonard Bernstein}}

In order to make the enormous amount of data from the \ac{cms} detector systems
meaningful, the particles created in collisions must be reconstructed. This
process begins with the identification of tracks in the inner tracker, tracks
in the Muon System, and clustered energy deposits in the calorimeters. From
these, a list of particles present in the event is then constructed using the
\ac{pf} algorithm~\cite{pf}. Finally, \emph{high-level objects}—such as jets
and \mET—are determined.

% talk MC to me

\section{Low-Level Physics Object Reconstruction}

The first step in the physics object reconstruction involves the identification
of \emph{low-level objects} on a detector-by-detector basis; information from
the different detector systems is not, at this stage, combined. These low-level
objects are charged particle tracks, primary vertices, and energy clusters in
the calorimeters.

\subsection{Charged Particle Tracks}

As discussed in \cref{sec:charged_particle_tracking}, the reconstruction of a
charged particle track allows the particle's \pT{} to be determined. In
addition, once a primary vertex has been found as described in
\cref{sec:primary_vertex}, the impact parameter of the charged particle can be
found.

Two metrics are defined to describe the quality of tracking: the percentage of
real tracks correctly reconstructed is the \emph{tracking efficiency} and the
percentage of~reconstructed tracks with no real track analogue is the
\emph{fake rate}. To achieve the best performance, the \ac{ctf} algorithm is
used~\cite{pixel_res}. The \ac{ctf} utilises a \ac{kf} to combine energy
deposits in the inner tracker—known as \emph{hits}—to form tracks.

% TODO: Talk about hits more in the detector chapter?

\subsubsection{Kalman filters}

The \ac{kf} is an algorithm allowing the creation of optimal predictions of
dynamic systems based on uncertain information~\cite{kf}. Consider a system
that can be entirely specified by \(\symbf{x} = \left(x₁,x₂,x₃,\ldots\right)\)
that evolves with \(k\). Only noisy measurements of \(\symbf{x}\) can be made,
and there exists a dynamic model of the system i.e.\ a method to predict the
value of \(\symbf{x}\) at \(k\) based on the value of \(\symbf{x}(k-1)\). A
\ac{kf} combines this prediction with noisy measurements of \(\symbf{x}(k)\) to
produce an estimate of \(\symbf{x}(k)\) that is more accurate than either
individually. For linear systems, a \ac{kf} will produce an optimal estimate:
no other technique will result in a smaller mean squared error.

\begin{figure}
    \centering \resizebox{\textwidth}{!}{\input{kalman.tex}} \caption{Diagram
    showing the basic operation of a \glsfmtshort{kf}. The initial state of the
    system, \(\symbf{x}(k-1)\), (grey) is used to make a prediction of
    \(\symbf{x}(k)\) (red). A \glsfmtshort{kf} is used to combine this
    prediction with a measurement of \(\symbf{x}(k)\) (blue) to create an
    estimate of \(\symbf{x}(k)\) more accurate than either the prediction or
    measurement (purple). This process can  then be repeated, using the
    \glsfmtshort{kf}'s estimate as the initial state to create a new estimate
    using a \glsfmtshort{kf} of \(\symbf{x}(k+1)\).}\label{fig:kalman}
\end{figure}

The estimate of \(\symbf{x}(k)\) obtained by the \ac{kf} can then be used to
predict \(\symbf{x}(k+1)\) using the dynamic model and, in combination with a
noisy measurement of \(\symbf{x}(k+1)\), can create an estimate of
\(\symbf{x}(k+1)\) using another \ac{kf}. An overview of this process is
presented in \cref{fig:kalman}. This estimate will be better than if a single
\ac{kf} had been used, starting at \(k\). Despite this, the second \ac{kf} only
needed information from the preceding state. This gives the algorithm a low
memory overhead and, combined with its relative simplicity, makes it ideal for
track fitting at \ac{cms}\@. In this context, \(\symbf{x}\) represents the
coordinates of a hit in the detector; additional candidate hits for a track are
added one at a time, with a \ac{kf} used to combine the predicted motion of the
particle with the hit. In this iterative manner, the uncertainty in a track
candidate decreases with the inclusion of each additional hit~\cite{kf_track}.

\subsubsection{The Combinatorial Track Finder algorithm}

The \ac{ctf} algorithm can be split broadly into four stages:
\begin{enumerate}
    \item Pairs and triplets of hits in the inner tracker are used to
        create initial estimates of helical charged particle tracks. This is
        known as \emph{track seeding}.
    \item These \emph{proto-tracks} are extrapolated and used to identify
        further candidate hits in other tracker layers, which are then combined
        with the proto-track using a \ac{kf}~\cite{kf_seed}. The algorithm
        searches for hits in each tracker layer, allowing~both zero and
        multiple hits in a layer. In the latter case, a track candidate is
        created for each hit, which can lead to track candidates sharing many
        of the same hits.
    \item Each track candidate is refit twice using a \ac{kf}: first starting
        from the innermost hit outwards
        and second starting from the outermost hit inwards (the \ac{kf} in the
        second fit is initialised with the result of the first). An average of
        the two resultant tracks is used to provide the optimal track
        parameters.
    \item Low-quality track candidates are discarded. The quality of a track
        candidate is determined by the quality of the fit, the number of
        intersecting layers that contain hits, and its compatibility with
        originating from a primary interaction vertex (see
        \cref{sec:primary_vertex}). The fraction of shared hits between two
        track candidates is calculated as
        \begin{equation}
            f_{\text{shared}} = \frac{N_{\text{shared}}}{\min\left(N₁,N₂\right)}
        \end{equation}
        where \(N_{\text{shared}}\) is the number of shared hits and \(N₁\)
        (\(N₂\)) is the number of hits used in forming the first (second) track
        candidate. If, for any pair of track candidates, \(f_{\text{shared}}\)
        exceeds \SI{19}{\percent} the track candidate with fewer hits is
        discarded (if \(N₁=N₂\) the track with the lowest quality fit is
        discarded). 
\end{enumerate}

Application of the \ac{ctf} is performed six times per event. Each iteration is
tuned differently: the location and quantity of hits needed to form
proto-tracks is changed as are the \pT{} and impact parameter requirements for
high-quality track candidates. Finally, the selection between track candidate
pairs with \(f_{\text{shared}}>\SI{19}{\percent}\) is performed on the
collection of all track candidates from every iteration.

\subsection{Primary Vertex Reconstruction}\label{sec:primary_vertex}

Reconstructed tracks that are well-fit, have two or more hits in the pixel
detector, five or more hits in the tracker, and a small impact parameter with
respect to the beam line are used to reconstruct \emph{primary interaction
vertices}~\cite{pixel_res}. A primary interaction vertex is a point where a
proton–proton collision took place. In any given event there will be multiple
such vertices, because of pileup. The primary interaction vertex with the
greatest total \pT{} is chosen as the \emph{primary vertex}, with those
remaining considered to be pileup vertices.

Primary interaction vertices are found by using a deterministic annealing
algorithm~\cite{deterministic_annealing} to cluster tracks according to the
\(z\)~coordinate of their closest approach to the beam axis. This is followed
by an adaptive vertex fit~\cite{avf} to determine the position of each possible
vertex from its associated tracks.

\subsection{Calorimeter Energy Clusters}

The \ac{hcal} and \ac{ecal} detectors are divided into \emph{cells}. Incident
particles create deposits in groups of neighbouring cells (\emph{clusters})
that must be identified to correctly reconstruct the location and energy of the
particle that caused the deposit.

The large cell size of the \ac{hf} renders clustering unnecessary: each
individual cell is considered a cluster. In the remaining calorimeters the
clustering algorithm begins by identifying \emph{seed cells}. Seed cells must
have deposited energy in excess of a threshold that varies by
subdetector and must contain more deposited energy than any neighbouring
cells. Clusters are grown from these seeds by adding cells~that share at least
one corner with a cell already in the cluster and contain deposited energy in excess of
two standard deviations from the expected electronics noise. This equates to
\SI{800}{\MeV} in the \ac{hcal}, \SI{80}{\MeV} in the \ac{ecal} barrel, and
\SI{300}{\MeV} in the \ac{ecal} end cap~\cite{pf}.

\section{Particle Flow}

The \ac{pf} algorithm~\cite{pf} uses information from all \ac{cms} detector
systems to reconstruct the particles present in an event. It begins with a
linking step, connecting tracks from the inner tracker (and potentially the
Muon System) to energy clusters in the calorimeter systems. The resulting
linked objects are known as \emph{blocks}. The \ac{pf} algorithm then attempts
to identify the particle responsible for each block in the following order:
muons, electrons, neutral hadrons, photons, and finally charged hadrons. Once a
block has been associated with a particular particle type it is removed from
further consideration.

\subsection{The Linking Algorithm}

Charged particle tracks found by the \ac{ctf} are projected into the
calorimeter systems. If the projected track lies within an identified
calorimeter cluster, the track and cluster are linked as a block. Clusters in
the \ac{ecal} and \ac{hcal} can be linked too, provided the former lies within
the boundary of the latter.

This process is complicated slightly by photons emitted as bremsstrahlung
radiation. Photon trajectories are not bent by the magnetic field and so will
propagate at a tangent to charged particle tracks. A charged particle will
release bremsstrahlung radiation when interacting with material in the inner
tracking system, hence a tangent is extrapolated from every hit in every track.
Any \ac{ecal} cluster whose location is consistent with one of these tangents
is flagged as the possible result of a bremsstrahlung photon.

\subsection{Muon Reconstruction}

Muon tracks, like any other, are reconstructed in the inner tracker using the
\ac{ctf}\@. An independent track reconstruction process is performed in
parallel using the muon system alone. This process is similar to that performed
in the inner tracker, using small seed track segments (much the same as
proto-tracks) constructed using hits in the \acp{dt} and \acp{csc}. Full tracks
are then built using a \ac{kf} that combines candidate hits in the \acp{dt},
the \acp{csc}, and the \acp{rpc}.

The existence of two sets of independently reconstructed muon tracks allows for
two separate methods of muon reconstruction:
\begin{description}
    \item[Global (outside-in)] All tracks reconstructed in the Muon System are
        projected inward, towards the inner tracker. They are then combined
        with the closest-matching track in the inner tracker using a \ac{kf}.
        The resulting complete block is a \emph{global muon}.
    \item[Tracker (inside-out)] All tracks in the inner tracker that satisfy
        \(\pT>\SI{0.5}{\GeV}\) and \({|\symbf{p}|>\SI{2.5}{\GeV}}\)
        are extrapolated to the Muon System. If a matching muon track segment
        is found the track is considered a \emph{tracker muon}.
\end{description}

A muon could be reconstructed as both a tracker muon and a global muon. To
account for this, any pairs of tracker muons and global muons that share the
same track in the inner tracking system are merged. Approximately
\SI{99}{\percent} of muons produced in a proton–proton collision within the
Muon System \(η\) acceptance are identified by \ac{pf} as either a global or
tracker muon~\cite{muon_perf}.

\subsubsection{Rochester Corrections}

Beyond the \ac{pf} muon reconstruction, corrections that apply a scale factor
to muon four-momentum known as the Rochester Corrections~\cite{rochester} are
recommended for all \ac{cms} analyses with muons in the final state. The scale
factor is derived from~a muon's charge, \pT, \(η\), and \(\varphi\). These
corrections correct for biases arising~from detector misalignment and errors in
the measurement of the magnetic field and alter the muon's \pT{} by
\({≲}\SI{2}{\percent}\) in the \tZq{} analysis.

\subsection{Electron Reconstruction}

Electrons are particularly susceptible to bremsstrahlung radiation, losing
\SIrange{33}{86}{\percent} of their energy to it in the inner
tracker~\cite{e_reco}. This poses a problem for electron reconstruction using a
\ac{kf}\@. While the \ac{kf} formalism does not assume Gaussian noise, it
only performs optimally in this case. Bremsstrahlung radiation, however,
produces uncertainties that are not and cannot be reasonably approximated by a
Gaussian, to the extent that electron reconstruction performance is
significantly degraded when using a \ac{kf} alone. To resolve this, a \ac{kf}
is used for an initial fit and is then refined with the more comprehensive—and
computationally expensive—\ac{gsf} algorithm~\cite{e_gsf}. Bremsstrahlung
radiation presents a further problem in the \ac{ecal}, namely the association
of all bremsstrahlung energy deposits from an electron to the correct source.
This is essential to fully reconstruct an electron's initial energy. The fact
that the electron has a curved trajectory while that of the photon's is
straight results in the energy deposits associated with a single electron
potentially covering a range over \(φ\). Clusters that fall into this pattern
are thus arranged into \emph{superclusters} through the use of two different
algorithms: the \emph{hybrid algorithm} in the \ac{ecal} barrel and the
\emph{multi algorithm} in the \ac{ecal} end cap~\cite{e_reco}.

Two different methods are used by the \ac{pf} algorithm to identify electron track
seeds~\cite{e_reco}:
\begin{description}
    \item[\Glsfmtshort{ecal}-driven] The energy and layout of a supercluster is
        used to calculate the associated electron's hits in the inner tracker.
        This is most effective for isolated, high-\pT{} electrons: nonisolated
        electrons (e.g.\ those within jets) can have other tracker hits
        incorrectly associated with them, and the bremsstrahlung distribution
        of a low-\pT{} electron can exceed the bounds of a single supercluster.
    \item[Tracker-driven] A \ac{kf} is used to fit tracks in the inner tracker,
        followed by a \ac{gsf} for tracks indicating significant bremsstrahlung
        energy loss. A discriminant from a \ac{bdt} is used to select electron
        tracks, using quantities such as the number and quality of tracks as
        reconstructed by both the \ac{kf} and \ac{gsf} as features. This method
        complements the shortcomings of the \ac{ecal}-driven procedure, as it
        is more suited to low-\pT{}, nonisolated electrons.
\end{description}

Once identified, the seeds created by each method are consolidated.
Subsequently, a final fit with a \ac{gsf} is performed utilising the
additional track information~afforded by the merged seed collection, allowing for
an improved reconstruction. An overall efficiency of more than
\(\SI{95}{\percent}\) was estimated using simulated \(\PZ→\Pe\Pe\)
decays~\cite{e_reco}.

\subsection{Photon and Hadron Reconstruction}

Reconstruction of isolated high-energy photons is performed simultaneously with
the electron reconstruction procedure described above. Superclusters in the
\Ac{ecal} with \(\ET>\SI{10}{\GeV}\) (where \(\ET=E\sin θ\,\)), no linked
track, and a photon-compatible energy ratio between the energy deposited in the
\ac{ecal} and matching deposits in the \ac{hcal} are reconstructed as photons.

After all tracker hits and calorimeter deposits associated with electrons,
muons, and isolated photons have been removed from further consideration, those
remaining are considered by \ac{pf} to be the product of either nonisolated
photons or hadrons. Within the tracker acceptance, leftover clusters in the
\ac{ecal} or \ac{hcal} with no associated tracks are classed as photons or
neutral hadrons, respectively. Remaining tracks with associated clusters in the
\ac{hcal} are considered to be charged hadrons, and can be linked with
compatible \ac{ecal} clusters.

Outside the tracker acceptance it is impossible to distinguish between charged
and neutral hadrons~\cite{pf}. In this region, compatible \ac{ecal} and
\ac{hcal} clusters are considered to originate from hadrons, while \ac{ecal}
clusters not linked with \ac{hcal} clusters are considered the result of
photons.

\section{High-Level Physics Object Reconstruction}

Once the \ac{pf} algorithm has successfully reconstructed the particles in an
event, higher-level objects can be constructed. These objects depend on the full
particle listing and include composite objects, such as jets, and properties of
the event as a whole, such as missing transverse energy.

\subsection{Jets}\label{sec:reco_jets}

Jets are cone-like sprays of hadronic particles originating from a
partons~\cite{PDG}, described previously in \cref{sec:strong_nuclear_force}. A
sequential recombination algorithm is used to identify jets, evaluating
entities (e.g.\ particles, pseudojets, labelled \(i,j\) herein) based on two
metrics:
\begin{equation}
    d_{\symup{B}\,i}=p_{\T\,i}^{2k}
\end{equation}
and
\begin{equation}
    d_{ij}=\min\left(p_{\T\,i}^{2k},p_{\T\,j}^{2k}\right)\frac{\ΔRᵢⱼ²}{R²}
\end{equation}
where
\begin{equation}\label{eq:delta_r}
    \ΔRᵢⱼ²=\left(yᵢ-yⱼ\right)² + \left(φᵢ-φⱼ\right)²,
\end{equation}
\(y\) is rapidity, and \(R\) is the \emph{jet size parameter}. The metrics
\(d_{ij}\) and \(d_{\symup{B}\,i}\) are distance metrics between entity pairs
and an entity and the beam (\(\symup{B}\)), respectively. The choice of \(k\)
distinguishes between three different jet-finding algorithms: the \kT{}
algorithm corresponds to \(k=1\)~\cite{kt}, the anti"~\kT{} algorithm to
\(k=-1\)~\cite{anti-kt}, and setting~\({k=0}\) defines the Cambridge–Aachen
algorithm~\cite{ca}.

Irrespective of the choice of \(k\), the algorithm proceeds by finding \(dᵢⱼ\)
and \(d_{\symup{B}\,i}\) for every pair of particles identified by the \ac{pf}
algorithm. If and only if \(dᵢⱼ<d_{\symup{B}\,i}\), then \(i\) and \(j\) are
combined and the process starts anew, otherwise \(i\) is classified as a
``jet''~and removed from the set of objects under consideration (a ``jet'' in
this case could be a single, isolated particle). When \(i\) and \(j\) are
combined, the resultant object has the following properties:
\begin{align}
    \pT = p_{\T\,i} + p_{\T\,j} &&
    y = \frac{p_{\T\,i}\,yᵢ+p_{\T\,j}\,yⱼ}{\pT} &&
    φ = \frac{p_{\T\,i}\,φᵢ+p_{\T\,j}\,φⱼ}{\pT}.
\end{align}

Jets identified with the anti-\kT{} algorithm using \ac{pf} objects are called
\emph{\ac{pf}~jets}. Making use of \ac{pf} objects provides a marked
improvement over relying exclusively on calorimeter energy clusters. Within a
jet, the typical energy distribution is \(\text{\SI{65}{\percent} charged
particles}:\allowbreak{}\text{\SI{25}{\percent}
photons}:\allowbreak{}\text{\SI{10}{\percent} neutral hadrons}\), and so
\SI{90}{\percent} of the particles present will not be observed in the
\ac{hcal}\@. This, combined with the poor resolution of the \ac{hcal}, leads to
the improvement seen when using anti-\kT{}.

At \ac{cms} the anti-\kT{} algorithm is used with a jet size parameter of
\(R=0.4\). Jets found using this configuration are referred to as
\emph{\case{AK4} jets}.

\subsubsection{Jet Energy Corrections}

Once the jets have been identified, they are subject to various \acp{jec} in
order to account for nonuniformity in the detector response in \(η\) and \pT{}.
These take the form of scale factors to a jet's four-momentum and are derived
using simulated data samples (see \cref{chap:sim}). In order of application,
the corrections are~\cite{jec}:
\begin{description}
    \item[\case{L1} Pileup Correction] Corrects for the presence of additional energy
        arising from pileup interactions and electronic noise.
    \item[\case{L2} Relative Jet Correction] An \(η\)-dependent scale factor
        aiming to make the detector response \(η\)-independent. Derived from
        dijet events with one jet in the barrel region, the \pT{} of the
        barrel jet is used to find the scale factor needed for the other jet.
    \item[\case{L3} Absolute Jet Correction] A \pT-dependent scale factor
        aiming to make the detector response \pT-independent. Derived using
        events containing a leptonic \PZ~boson decay (\({\PZ→\Plp\Plm}\)) in
        association with one or more jets (\Zjets{} events), with the goal to
        set the scale factors such that a flat response in \pT{} is observed.
    \item[\case{L2L3} Residual] Final corrections, applied to data to reconcile
        any remaining discrepancies between simulated events and data.
\end{description}
The uncertainties associated with these corrections form a systematic
uncertainty in the \tZq{} analysis discussed further in \cref{sec:jec_syst}.

\subsubsection{Identification of jets from b~hadron decays}

The structure of the \ac{ckm} in \cref{eq:ckm_matrix} means that the
\({\Pb{}→\Pc\PW}\) and \({\Pb{}→\Pu\PW}\) decays are suppressed, resulting in a
longer lifetime for hadrons containing bottom quarks. This can be exploited in
order to specifically identify (\emph{tag}) jets originating from bottom quarks
(known as \Pb~jets). The longer lifetime of \Pb~hadrons often allows the
identification of a \emph{secondary vertex} a measurable distance from the
primary vertex where the \Pb~hadron decayed. These secondary vertices are
identified by the \ac{ivf} algorithm~\cite{ivf,btag}.

% The \ac{ivf} algorithm~\cite{ivf} is used to find these secondary vertices,
% using tracks with \(\pT>\SI{0.8}{\GeVc}\) and longitudinal impact parameter
% \(d_z<\SI{0.3}{\cm}\). The \ac{ivf} then agglomerates tracks of
% \(d₀>\SI{50}{\micro\metre}\) with adjacent tracks, using an adaptive vertex
% fitter on the resulting cluster to locate the vertex. Vertices with small
% \(d_z\) are pruned, and tracks still belonging to multiple secondary vertices
% are disambiguated. Finally, the resulting clusters are refitted and the
% secondary vertex candidates are subject to a number of selection
% criteria~\cite{btag}:
% \begin{itemize}
%     \item The candidate must not have more than \SI{80}{\percent} of its tracks
%         compatible with the primary vertex
%     \item The difference in rapidity between the flight direction of the
%         secondary vertex and an associated jet must be \({<}0.3\)
%     \item The 2D flight distance—the distance between the primary and
%         secondary vertex on the transverse plane—must be
%         \SIrange{0.1}{25}{\mm}
%     \item The fractional uncertainty in the 2D flight difference should
%         be \({≤}2\)
%     \item The mass of the secondary vertex should be \({>}\SI{6.5}{\GeV}\) and
%         not be within \SI{50}{\GeV} of the \PKzS~mass
%         (\SI{498}{\MeV}~\cite{PDG})
% \end{itemize}

\ac{cms} has developed multiple \btag{}ging algorithms to classify \bjet{}s.
The flagship \btag{}gging algorithm for data taken in 2016 and 2017 was
\ac{csvv2}~\cite{btag}. \Ac{csvv2} makes use of \ac{ml} techniques, leveraging
\pgls{mlp} and using observables associated with both the secondary and primary
vertices as features. It is trained separately on three categories of jet:
those for which a secondary vertex has been fully reconstructed; those for
which a secondary vertex could not be reconstructed but have a pseudo-secondary
vertex identified from tracks with a significant, positive and compatible
impact parameter; and jets with neither a pseudo-~nor fully reconstructed
secondary vertex. Two discriminants are produced—one between \Pb~jets and
\Pc~jets (jets originating from charm quarks in charm hadron decays) and the
other between \Pb~jets and all other jets (i.e.\ \emph{light jets})—and
combined to give an overall \ac{csvv2} discriminant.

% \begin{table}
%     \centering
%     \caption{Working points of the \ac{csvv2} \btag{}ging algorithm in 2016 and
%     2017 data.}
%     \begin{tabular}{lr@{\extracolsep{0pt}.}lr@{\extracolsep{0pt}.}lr@{\extracolsep{0pt}.}l}
%         \toprule
%         Year & \multicolumn{2}{c}{Loose \glsfmtshort{wp}} & \multicolumn{2}{c}{Medium \glsfmtshort{wp}} & \multicolumn{2}{c}{Tight \glsfmtshort{wp}}\tabularnewline
%         \midrule
%         \midrule
%         2016 & 0&5803 & 0&8838 & 0&9693\tabularnewline
%         \midrule
%         2017 & 0&460 & 0&800 & 0&935\tabularnewline
%         \bottomrule
%     \end{tabular}\label{tab:csvv2_wp}
% \end{table}

\begin{table}
    \centering \caption{The efficiency (i.e.\ the recall, see
        \cref{sec:classifier_evaluation}), \(ε_{\Pb}\); \typeI~error rate
        for \Pc~jets, \(α_{\Pc}\); and \typeI~error rate for light jets,
        \(α_{\Pu\Pd\Ps\Pg}\), for \(\pT>\SI{20}{\GeV}\) jets achieved by
        \glsfmtshort{csvv2} in simulated \ttbar{} events. Values taken
    from~\cite{btag}.}
    \begin{tabular}{lsss}
        \toprule
        \multicolumn{1}{c}{\glsfmtshort{csvv2} Working Point} & \multicolumn{1}{c}{\(ε_{\Pb}\)} & \multicolumn{1}{c}{\(α_{\Pc}\)} & \multicolumn{1}{c}{\(α_{\Pu\Pd\Ps\Pg}\)}\tabularnewline
        \midrule
        Loose & \SI{84}{\percent} & \SI{39}{\percent} & \SI{8.3}{\percent}\tabularnewline
        Medium & \SI{66}{\percent} & \SI{13}{\percent} & \SI{0.8}{\percent}\tabularnewline
        Tight & \SI{46}{\percent} & \SI{2.6}{\percent} & \SI{0.1}{\percent}\tabularnewline
        \bottomrule
    \end{tabular}\label{tab:csvv2_perf}
\end{table}

In analyses the requirements placed on a \btag{}ger's discriminant are set at
one of three possible \acp{wp}, each defined by the associated \typeI~error
rate: \SI{10}{\percent} at the \emph{loose \ac{wp}}, \SI{1}{\percent} at the
\emph{medium \ac{wp}}, and \SI{0.1}{\percent} at the \emph{tight
\ac{wp}}~\cite{btag}. A~summary of \ac{csvv2}'s performance on simulated
\ttbar{} samples is presented in \cref{tab:csvv2_perf}.

\subsection{Missing Transverse Energy}\label{sec:met}

Weakly interacting particles like neutrinos, including potential but as-yet
unobserved particles such as those that may compose dark matter, do not
sufficiently interact with the \ac{cms} detector to be observable. Their
presence can, however, be inferred through conservation of momentum: any
significant difference in the total momentum in the final state when compared
to the initial state can be attributed to undetected particles. A substantial
fraction of a collision's debris will escape along the beamline, making
reliable reconstruction of the missing \(p_z\) impossible. This is not the case
for \pT{}, the total of which must be zero in the initial and, therefore, final
state. The missing transverse energy, \mET{}, is thus defined
as~the component transverse to the beam of the total momentum of all particles
in the~final state (while this technically constitutes the missing transverse
momentum, this is considered to be \({≈}\mET\) in the ultrarelativistic limit).

\Ac{pf} objects can be used to determine the \mET{} on an event-by-event basis,
in which case it is bestowed the moniker \emph{\ac{pf}~\mET}. Energy inside
unclustered energy deposits—energy deposits in the calorimeters excluded from
clusters due to low \pT{} or isolation—are also included in this calculation,
counting against the \mET{} in an event. Studies performed on \Zjets{} events,
which should contain zero \mET{}, have been used to demonstrate that this
method is the most accurate available to the \ac{cms}
experiment~\cite{met_perf}. These studies have also identified the need for the
\emph{\typeI~corrections} to the \ac{pf} \mET{}, where the \acp{jec} are
propagated to the \mET{} calculation. The uncertainty associated with these and
other corrections to the \mET{} in the \tZq{} analysis are discussed in
\cref{sec:syst_met}. It is assumed henceforth that any mention of \mET{} refers
to \ac{pf}~\mET{}.

\end{document}
