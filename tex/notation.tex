%! TEX program = lualatex

\documentclass[../thesis.tex]{subfiles}

\graphicspath{{fig/}{../fig/}{fig/notation/}{../fig/notation/}}

\makeatletter
\def\input@path{{fig/}{../fig}{../fig/notation/}{fig/notation/}}
\makeatother

\begin{document}

\chapter{Notes on Notation}\label{apx:notation}

In general, the notation used in this thesis follows the
\mbox{\case{\textls{ISO}~80000–2:2009}} standard~\cite{ISO80000-2}. Additional
notation used is defined in the sections below.

\section{Index Notation}\label{sec:index_notation}

In this thesis the notation of the Ricci calculus~\cite{ricci_calculus} is
used. A covariant four-vector is represented as a majuscule Latin letter with a
minuscule Greek lower index: \(A_{μ}\). The Greek letter represents the four
indexes of spacetime, \(\{0,1,2,3\}\), where \(0\)~is the time component and
\(1,2,3\) the spatial components. A contravariant vector is~represented by a
raised index, e.g.\ \(A^{μ}\), and tensors by multiple indices, e.g.\
\(F^{μv}\).

Indices can be raised or lowered by the Minkowski metric~\cite{Peskin:1995}:
\begin{align}
    \begin{aligned}
        {A^{ξ}}_{ν…} &= η^{ξμ}A_{μν…} \\ A_{μν…} &= η_{μξ}{A^{ξ}}_{ν…}
    \end{aligned}
    &&
    \begin{aligned}
        η_{μν} = \begin{pmatrix} 1 & 0 & 0 & 0 \\ 0 & -1 & 0 & 0 \\ 0 & 0 & -1 & 0 \\ 0 & 0 & 0 & -1 \end{pmatrix}.
    \end{aligned}
\end{align}
The same symbol appearing as an upper index and lower index in a single term
implies a summation
\begin{align}
    A_{μ}B^{μ} = ∑_{μ}A_{μ}B^{μ} && A^{μ}B_{μ} = ∑_{μ}A^{μ}B_{μ}
\end{align}
and other combinations of repeated indices are ill-formed.  The four-gradient,
\(∂^{μ}\), is defined~\cite{Cheng:1995}
\begin{equation}
    ∂^{μ} = (∂_{t},-\symbf{∇}) = \left(\frac{∂}{∂t},-\frac{∂}{∂x},-\frac{∂}{∂y},-\frac{∂}{∂z}\right).
\end{equation}

\section{Dirac Matrices}\label{sec:dirac_matrices}

The Dirac matrices, \(γ^{μ}\), are defined as~\cite{Peskin:1995}
\begin{align}
    γ₀ = \begin{pmatrix} I₂ & 0 \\ 0 & -I₂ \end{pmatrix} && γᵢ = \begin{pmatrix} 0 & σᵢ \\ -σᵢ & 0 \end{pmatrix}
\end{align}
where \(σⁱ\) are the Pauli matrices~\cite{Shaw:2008}
\begin{align}
    σ₁ = \begin{pmatrix} 0 & 1 \\ 1 & 0 \end{pmatrix} && σ₂ = \begin{pmatrix} 0 & -\I \\ \I & 0 \end{pmatrix} && σ₃ = \begin{pmatrix} 1 & 0 \\ 0  & -1 \end{pmatrix}
\end{align}
and \(I₂\) is the \(2×2\) identity matrix.
The fifth gamma matrix, \(γ₅\), is~\cite{Cheng:1995}
\begin{equation}
    γ₅ = \I γ₀γ₁γ₂γ₃ = \begin{pmatrix} 0 & I₂ \\ I₂ & 0 \end{pmatrix}.
\end{equation}

Dirac matrices can be used to transform a four-vector into a linear operator on
spinor fields, \(ψ\). This is represented using the \emph{Feynman slash}
notation~\cite{feynman_slash},
\begin{equation}
    \slashed{∂} = γ^{μ}∂_{μ}.
\end{equation}
The matrix \(γ⁰\) is also used to relate the barred an unbarred forms of spinor
fields~\cite{Peskin:1995}:
\begin{equation}
    \bar{ψ} = ψ^{†}γ⁰
\end{equation}
where \(ψ^{†}\) is the Hermitian adjoint of \(ψ\)~\cite{Cheng:1995}.

\section{Probability}\label{sec:probability_notation}

When discussing probability, random variables are set in Latin majuscule:
\(X\), \(Y\), \(Z\), etc.\ with specific realisations set in the corresponding
minuscule: \(x\), \(y\), \(z\), etc. This extends to vectors, that is,
\(\symbf{x}\) represents a specific realisation of the random vector
\(\symbf{X}\).

The distribution of a random variable, \(Z\), can be defined by a continuous
probability distribution of its realisations, \(P(z)\). In the case of the
Gaussian distribution,
\begin{equation}
    P\left(z|μ_z,σ_z\right) = \frac{1}{σ_{z}√{2\π}}\exp\left(-\frac{\left(z-μ_z\right)²}{2σ_{z}²}\right)
\end{equation}
where \(μ_z\) is the mean of \(Z\) (i.e.\ \(μ_z=\symup{E}[Z]\)), and \(σ_z\) is
the standard deviation of \(Z\). A~shorthand for this is
\begin{equation}
    Z\sim\symcal{N}\left(μ_z,σ_z\right)
\end{equation}
where the tilde denotes `is distributed as' and
\(\symcal{N}\left(μ_z,σ_z\right)\) represents a Gaussian distribution of mean
\(μ_z\) and standard deviation \(σ_z\).

\end{document}
